FROM node:11-alpine
WORKDIR /app
COPY server/package.json /app
RUN npm install
COPY ./server /app
CMD node index.js
EXPOSE 7000