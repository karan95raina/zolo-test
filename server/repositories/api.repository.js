const redis = require('redis');
const config = require('../config/keys');
const env = process.env.FLASH_APP_ENV;

let redisCredentials = {
  host: config[env].redis_host,
  port: config[env].redis_port
};
if (env !== 'local') {
  redisCredentials.auth_pass = config[env].redis_password;
}
if (env === 'production') {
  redisCredentials.tls = {checkServerIdentity: () => undefined};
}

const client = redis.createClient(redisCredentials);
client.on('error', (error) => {
  console.log(error);
});
client.on('connect', function () {
  console.log('Redis client connected');
});

client.select((config[env].redis_db) ? config[env].redis_db : 0);

const {promisify} = require('util');

// make the get and set calls async to support promises and await
const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);
const delAsync = promisify(client.del).bind(client);
const getHashAsync = promisify(client.hmget).bind(client);

const getValuesFromRedis = async (key) => {
  try {
    const response = await getAsync(key);
    return response;
  } catch (e) {
    throw e;
  }
};

const putValueInRedis = async (key, value, expiry) => {
  try {
    let response = {};
    if (expiry > 0) {
      response = await setAsync(key, value, 'EX', expiry);
    } else {
      response = await setAsync(key, value);
    }
    return response;
  } catch (e) {
    throw e;
  }
};

const removeKeyFromRedis = async (key) => {
  try {
    const response = await delAsync(key);
    return response;
  } catch (e) {
    throw e;
  }
};

const getValueInHashFromRedis = async (key, field) => {
  try {
    const response = await getHashAsync(key, field);
    return response;
  } catch (e) {
    throw e;
  }
};

module.exports = {
  getValuesFromRedis,
  putValueInRedis,
  removeKeyFromRedis,
  getValueInHashFromRedis
};