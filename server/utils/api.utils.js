const request = require('request-promise-native');
const config = require('../config/keys');
const env = process.env.FLASH_APP_ENV;

const createEndPoint = (url, query) => {
  try {
    if (query) {
      let queryString = Object.keys(query).map(key => key + '=' + query[key]).join('&');
      let endpoint = `${url}?${queryString}`;
      return endpoint;
    } else {
      return url;
    }
  } catch (e) {
    throw e;
  }
};

const getDatafromApi = async (endpoint, headers, productKey) => {
  try {
    let options = {
      url: `${config[env].api_url[productKey]}${endpoint}`
    };
    // if token present in the headers
    if (headers && headers.token) {
      options.headers = {
        token: headers.token
      }
    }
    const response = await request(options);
    return response;
  } catch (e) {
    throw e;
  }
};

module.exports = {
  createEndPoint,
  getDatafromApi
};