const express = require('express');
const router = express.Router();
const ApiController = require('../controllers/api.controller');

// api endpoints
router.get(['/crm/allZoloEmployees'], ApiController.validateToken, ApiController.handleApiRequest);
router.get('*', ApiController.handleApiRequest);

module.exports = router;