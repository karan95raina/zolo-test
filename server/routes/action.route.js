const express = require('express');
const router = express.Router();
const ApiController = require('../controllers/api.controller');

// purge cache
router.post('/purge', ApiController.purgeCache);

module.exports = router;