
module.exports = {
  /**
  * Application configuration section
  * http://pm2.keymetrics.io/docs/usage/application-declaration/
  */
  apps: [
    {
      name: 'flash',
      script: './server/index.js',
      cwd: process.cwd(),
      env: {
        COMMON_VARIABLE: 'true'
      },
      env_production: {
        "FLASH_PORT": process.env.FLASH_PRODUCTION_PORT,
        "FLASH_APP_ENV": "production",
        "FLASH_PRODUCTION_REDIS_HOST": process.env.FLASH_PRODUCTION_REDIS_HOST,
        "FLASH_PRODUCTION_REDIS_PORT": process.env.FLASH_PRODUCTION_REDIS_PORT,
        "FLASH_PRODUCTION_REDIS_PASSWORD": process.env.FLASH_PRODUCTION_REDIS_PASSWORD,
        "FLASH_PRODUCTION_REDIS_DB": process.env.FLASH_PRODUCTION_REDIS_DB,
        "FLASH_PRODUCTION_API_URL": process.env.FLASH_PRODUCTION_API_URL,
        "FLASH_PRODUCTION_TOKEN_MAP": process.env.FLASH_PRODUCTION_TOKEN_MAP,
        "FLASH_PRODUCTION_ANDROID_TOKEN_MAP": process.env.FLASH_PRODUCTION_ANDROID_TOKEN_MAP,
        "FLASH_PRODUCTION_HERMES_URL": process.env.FLASH_PRODUCTION_HERMES_URL
      },
      env_staging: {
        "FLASH_PORT": process.env.FLASH_STAGING_PORT,
        "FLASH_APP_ENV": "staging",
        "FLASH_STAGING_REDIS_HOST": process.env.FLASH_STAGING_REDIS_HOST,
        "FLASH_STAGING_REDIS_PORT": process.env.FLASH_STAGING_REDIS_PORT,
        "FLASH_STAGING_REDIS_PASSWORD": process.env.FLASH_STAGING_REDIS_PASSWORD,
        "FLASH_STAGING_REDIS_DB": process.env.FLASH_STAGING_REDIS_DB,
        "FLASH_STAGING_API_URL": process.env.FLASH_STAGING_API_URL,
        "FLASH_STAGING_TOKEN_MAP": process.env.FLASH_STAGING_TOKEN_MAP,
        "FLASH_STAGING_ANDROID_TOKEN_MAP": process.env.FLASH_STAGING_ANDROID_TOKEN_MAP,
        "FLASH_STAGING_HERMES_URL": process.env.FLASH_STAGING_HERMES_URL
      },
      env_development: {
        "FLASH_PORT": process.env.FLASH_DEVELOPMENT_PORT,
        "FLASH_APP_ENV": "development",
        "FLASH_DEVELOPMENT_REDIS_HOST": process.env.FLASH_DEVELOPMENT_REDIS_HOST,
        "FLASH_DEVELOPMENT_REDIS_PORT": process.env.FLASH_DEVELOPMENT_REDIS_PORT,
        "FLASH_DEVELOPMENT_REDIS_PASSWORD": process.env.FLASH_DEVELOPMENT_REDIS_PASSWORD,
        "FLASH_DEVELOPMENT_REDIS_DB": process.env.FLASH_DEVELOPMENT_REDIS_DB,
        "FLASH_DEVELOPMENT_API_URL": process.env.FLASH_DEVELOPMENT_API_URL,
        "FLASH_DEVELOPMENT_TOKEN_MAP": process.env.FLASH_DEVELOPMENT_TOKEN_MAP,
        "FLASH_DEVELOPMENT_ANDROID_TOKEN_MAP": process.env.FLASH_DEVELOPMENT_ANDROID_TOKEN_MAP,
        "FLASH_DEVELOPMENT_HERMES_URL": process.env.FLASH_DEVELOPMENT_HERMES_URL
      },
      env_preprod: {
        "FLASH_PORT": process.env.FLASH_PREPROD_PORT,
        "FLASH_APP_ENV": "development",
        "FLASH_PREPROD_REDIS_HOST": process.env.FLASH_PREPROD_REDIS_HOST,
        "FLASH_PREPROD_REDIS_PORT": process.env.FLASH_PREPROD_REDIS_PORT,
        "FLASH_PREPROD_REDIS_PASSWORD": process.env.FLASH_PREPROD_REDIS_PASSWORD,
        "FLASH_PREPROD_REDIS_DB": process.env.FLASH_PREPROD_REDIS_DB,
        "FLASH_PREPROD_API_URL": process.env.FLASH_PREPROD_API_URL,
        "FLASH_PREPROD_TOKEN_MAP": process.env.FLASH_PREPROD_TOKEN_MAP,
        "FLASH_PREPROD_ANDROID_TOKEN_MAP": process.env.FLASH_PREPROD_ANDROID_TOKEN_MAP,
        "FLASH_PREPROD_HERMES_URL": process.env.FLASH_PREPROD_HERMES_URL
      }
    }
  ]
};
