const ApiRepository = require('../repositories/api.repository');
const ApiUtils = require('../utils/api.utils');
const volatileEndpoints = require('../config/volatileEndpoints');

const getApiResponse = async (productKey, endpoint, path, headers) => {
  try {
    const key = `${productKey.slice(1)}:${endpoint}`;
    const redisResponse = await ApiRepository.getValuesFromRedis(key);
    if (redisResponse) {
      return redisResponse;
    } else {
      console.log('make an API call to fetch the data');
      const response = await ApiUtils.getDatafromApi(endpoint, headers, productKey);
      let expiry = 0;
      if(volatileEndpoints.indexOf(path) > -1) {
        const now = new Date();
        const target = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1, 0 ,0 ,0);
        expiry = Math.floor((target.getTime() - now.getTime()) / 1000);
      }
      await ApiRepository.putValueInRedis(key, response, expiry);
      return response;
    }
  } catch (e) {
    throw e;
  }
};

const purgeCacheForKey = async (key) => {
  try {
    const response = await ApiRepository.removeKeyFromRedis(key);
    return response;
  } catch (e) {
    throw e;
  }
};

// get error message when header is missing
const getErrorMessage = async (productKey, path, query) => {
  try {
    const endpoint = ApiUtils.createEndPoint(path, query);
    const key = `${productKey.slice(1)}:error`;
    // check if message exists in redis
    const redisResponse = await ApiRepository.getValuesFromRedis(key);
    if (redisResponse) {
      return JSON.parse(redisResponse);
    } else {
      // get error message from api
      const response = await ApiUtils.getDatafromApi(endpoint);
      // store value in redis
      await ApiRepository.putValueInRedis(key, response);
      return JSON.parse(response);
    }
  } catch (e) {
    throw e;
  }
};

module.exports = {
  getApiResponse,
  purgeCacheForKey,
  getErrorMessage
};