if (process.env.FLASH_APP_ENV === 'production') {
  const tracer = require('dd-trace').init();
}
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const config = require('./config/keys');
const env = process.env.FLASH_APP_ENV;
const port = config[env].port;
const PlayZeloRouter = require('./routes/playzelo.route');
const HermesRouter = require('./routes/hermes.route');
const ActionsRouter = require('./routes/action.route');

// tracing flash requests via datadog
const tracer = require('dd-trace').init({
  analytics: true
});

app.use(cors());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());
 
app.use(function (req, res, next) {
  res.setHeader('Content-Type', 'application/json');
  next();
});

// route for playzelo endpoints
app.use(['/flash/playzelo','/playzelo'], PlayZeloRouter);

// route for hermes
app.use(['/flash/hermes','/hermes'], HermesRouter);

// route for internal action endpoints
app.use(['/flash/actions','/actions'], ActionsRouter);

// test ping route
app.get(['/flash/home','/home'], (req, res, next) => {
  res.send('Caching Layer is up!');
});

// start the server and make it listen on the given port
app.listen(port, () => {
  console.log(`listening on ${port}`);
});