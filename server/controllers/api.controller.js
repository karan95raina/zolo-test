const ApiService = require('../services/api.service');
const ApiUtils = require('../utils/api.utils');
const ApiRepo = require('../repositories/api.repository');
const config = require('../config/keys');
const env = process.env.FLASH_APP_ENV;

const handleApiRequest = async (req, res) => {
  try {
    const endpointWithQuery = ApiUtils.createEndPoint(req.path, req.query);
    const response = await ApiService.getApiResponse(req.baseUrl, endpointWithQuery, req.path, req.headers);
    return res.status(200).send(response);
  } catch (e) {
    return res.status(500).send(e.message);
  }
};

const purgeCache = async (req, res) => {
  try {
    const key = req.body.redis_key;
    const status = await ApiService.purgeCacheForKey(key);
    let response = {};
    if (status) {
      response = {
        error: false,
        message: "The value for the given key has been purged successfully."
      }
    } else {
      response = {
        error: true,
        message: "There was an error while purging the value for the given key."
      }
    }
    return res.status(200).send(response);
  } catch (e) {
    return res.status(500).send(e.message);
  }
};

// validate user token
const validateToken = async (req, res, next) => {
  try {
    // if token is sent in headers
    if (req.headers['token'] || req.query['token']) {
      const token = (req.headers['token'])?req.headers['token']:req.query['token'];
      // if token is present in the database
      const response = await ApiRepo.getValueInHashFromRedis(config[env].token_map, token);
      const response2 = await ApiRepo.getValueInHashFromRedis(config[env].android_token_map, token);
      if (response[0] || response2[0]) {
        // remove token from the query
        if (req.query['token']) {
          delete req.query['token'];
        }
        // continue
        next();
      } else {
        // get error message
        const authFailedMessage = await ApiService.getErrorMessage(req.path, req.query);
        // return error message
        return res.json(authFailedMessage);
      }
    }
    else {
      // get error message
      const authFailedMessage = await ApiService.getErrorMessage(req.baseUrl, req.path, req.query);
      // return error message
      return res.json(authFailedMessage);
    }
  } catch (e) {
    // return error message
    return res.status(500).send(e.message);
  }
};

module.exports = {
  handleApiRequest,
  purgeCache,
  validateToken
};